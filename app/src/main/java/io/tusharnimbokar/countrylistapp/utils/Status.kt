package io.tusharnimbokar.countrylistapp.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}