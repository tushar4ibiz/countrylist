package io.tusharnimbokar.countrylistapp.ui.main.view

import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.GenericRequestBuilder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.StreamEncoder
import com.bumptech.glide.load.resource.file.FileToStreamDecoder
import com.caverock.androidsvg.SVG
import io.tusharnimbokar.countrylistapp.R
import io.tusharnimbokar.countrylistapp.data.model.CountryListItem
import io.tusharnimbokar.countrylistapp.databinding.ActivityCountryDetailsBinding
import io.tusharnimbokar.countrylistapp.ui.main.adapter.svg.SvgDecoder
import io.tusharnimbokar.countrylistapp.ui.main.adapter.svg.SvgDrawableTranscoder
import io.tusharnimbokar.countrylistapp.ui.main.adapter.svg.SvgSoftwareLayerSetter
import io.tusharnimbokar.countrylistapp.utils.FetchImageSVG
import java.io.InputStream

class CountryDetailsActivity : AppCompatActivity() {
    lateinit var binding: ActivityCountryDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_country_details)


        val actionBar: ActionBar? = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        /*Check intent has country*/
        if (intent.hasExtra("country")) {

            var countryItem = intent.getSerializableExtra("country") as CountryListItem
            binding.country = countryItem
            actionBar?.title = countryItem.name
            FetchImageSVG.fetchSvg(this, countryItem.flag, binding.imgFlag)
            binding.tvRegion.text = "Region : ${countryItem.region}\nCurrency Name :" +
                    " ${countryItem.currencies[0].name}\nCode : ${countryItem.currencies[0].code}\n" +
                    "Symbol : ${countryItem.currencies[0].symbol}\nCapital : ${countryItem.capital}"

            var lang = ""
            countryItem.languages.forEach { lang = lang + " ${it.name}" }
            binding.tvLang.text = "Language : $lang"

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}