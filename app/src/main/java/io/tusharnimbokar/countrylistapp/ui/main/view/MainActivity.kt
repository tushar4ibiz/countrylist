package io.tusharnimbokar.countrylistapp.ui.main.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import io.tusharnimbokar.countrylistapp.R
import io.tusharnimbokar.countrylistapp.data.Api.ApiHelper
import io.tusharnimbokar.countrylistapp.data.Api.ApiServiceImpl
import io.tusharnimbokar.countrylistapp.data.model.CountryListItem
import io.tusharnimbokar.countrylistapp.databinding.ActivityMainBinding
import io.tusharnimbokar.countrylistapp.ui.base.ViewModelFactory
import io.tusharnimbokar.countrylistapp.ui.main.adapter.CountryListAdapter
import io.tusharnimbokar.countrylistapp.ui.main.viewmodel.MainViewModel
import io.tusharnimbokar.countrylistapp.utils.Status

class MainActivity : AppCompatActivity() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var adapter: CountryListAdapter
    lateinit var binding: ActivityMainBinding
    private var exit = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupUI()
        setupViewModel()
        setupObserver()
    }

    private fun setupUI() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adapter =
            CountryListAdapter { selectedItem: CountryListItem -> listItemClick(selectedItem) }
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                binding.recyclerView.context,
                (binding.recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        binding.recyclerView.adapter = adapter
    }

    private fun listItemClick(country: CountryListItem) {
        startActivity(Intent(this, CountryDetailsActivity::class.java).putExtra("country", country))
    }

    private fun setupViewModel() {
        mainViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(ApiServiceImpl()))
        ).get(MainViewModel::class.java)
    }

    private fun setupObserver() {
        mainViewModel.getCountryList().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    it.data?.let { users -> renderList(users) }
                    binding.recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun renderList(country: List<CountryListItem>) {
        adapter.setList(country)
        adapter.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        if (exit) {
            finishAffinity()
        } else {
            Toast.makeText(
                this, "Press Back again to Exit.",
                Toast.LENGTH_SHORT
            ).show()
            exit = true
            Handler().postDelayed(Runnable { exit = false }, 3 * 1000)
        }
    }

}