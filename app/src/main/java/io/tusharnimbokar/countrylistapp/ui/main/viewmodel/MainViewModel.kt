package io.tusharnimbokar.countrylistapp.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.tusharnimbokar.countrylistapp.data.mainRepository.MainRepository
import io.tusharnimbokar.countrylistapp.data.model.CountryListItem
import io.tusharnimbokar.countrylistapp.utils.Resource

class MainViewModel (private val mainRepository: MainRepository) : ViewModel() {

    private val country = MutableLiveData<Resource<List<CountryListItem>>>()
    private val compositeDisposable = CompositeDisposable()

    init {
        fetchCountryList()
    }

    private fun fetchCountryList() {
        country.postValue(Resource.loading(null))
        compositeDisposable.add(
            mainRepository.getCountry()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ userList ->
                    country.postValue(Resource.success(userList))
                }, { throwable ->
                    country.postValue(Resource.error("Something Went Wrong", null))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getCountryList(): LiveData<Resource<List<CountryListItem>>> {
        return country
    }

}