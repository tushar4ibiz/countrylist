package io.tusharnimbokar.countrylistapp.ui.main.adapter

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.GenericRequestBuilder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.StreamEncoder
import com.bumptech.glide.load.resource.file.FileToStreamDecoder
import com.caverock.androidsvg.SVG
import io.tusharnimbokar.countrylistapp.R
import io.tusharnimbokar.countrylistapp.data.model.CountryListItem
import io.tusharnimbokar.countrylistapp.databinding.ItemLayoutBinding
import io.tusharnimbokar.countrylistapp.ui.main.adapter.svg.SvgDecoder
import io.tusharnimbokar.countrylistapp.ui.main.adapter.svg.SvgDrawableTranscoder
import io.tusharnimbokar.countrylistapp.ui.main.adapter.svg.SvgSoftwareLayerSetter
import java.io.InputStream

class CountryListAdapter(
    private val clickListener: (CountryListItem) -> Unit
) :
    RecyclerView.Adapter<myViewHolderQuiz>() {
    lateinit var mContext: Context
    private val countryList = ArrayList<CountryListItem>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolderQuiz {
        mContext = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemLayoutBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_layout, parent, false)
        return myViewHolderQuiz(binding, mContext)
    }

    override fun getItemCount(): Int {
        return countryList.size
    }

    override fun onBindViewHolder(holder: myViewHolderQuiz, position: Int) {
        holder.bind(countryList[position], clickListener)
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    fun setList(quiz: List<CountryListItem>) {
        countryList.clear()
        countryList.addAll(quiz)
    }

}

class myViewHolderQuiz(val binding: ItemLayoutBinding, var mContext: Context) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(
        country: CountryListItem,
        clickListener: (CountryListItem) -> Unit
    ) {
        binding.tvCountryName.text = country.name
        binding.tvCountryCapital.text = "Capital : ${country.capital}"

        /*Item Click Listener*/
        binding.container.setOnClickListener {
            clickListener(country)
        }



        /*Code copy from internet*/
        /*To display svg image*/
        val requestBuilder: GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> =
            Glide.with(mContext)
                .using<Uri, InputStream>(
                    Glide.buildStreamModelLoader(Uri::class.java, mContext),
                    InputStream::class.java
                )
                .from(Uri::class.java)
                .`as`<SVG>(SVG::class.java)
                .transcode<PictureDrawable>(SvgDrawableTranscoder(), PictureDrawable::class.java)
                .sourceEncoder(StreamEncoder())
                .cacheDecoder(FileToStreamDecoder<SVG>(SvgDecoder()))
                .decoder(SvgDecoder())
                .listener(SvgSoftwareLayerSetter<Uri>())

        requestBuilder.diskCacheStrategy(DiskCacheStrategy.NONE)
            .load(Uri.parse(country.flag)).into(binding.imgFlag)
    }


}