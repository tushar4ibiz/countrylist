package io.tusharnimbokar.countrylistapp.data.model.user

data class Notification(
    val nameValuePairs: NameValuePairsXX
)