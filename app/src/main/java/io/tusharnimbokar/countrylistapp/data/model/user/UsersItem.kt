package io.tusharnimbokar.countrylistapp.data.model.user

data class UsersItem(
    val avatar: String,
    val email: String,
    val id: String,
    val mobileNumber: String,
    val name: String,
    val nameValuePairs: NameValuePairs,
    val password: String
)