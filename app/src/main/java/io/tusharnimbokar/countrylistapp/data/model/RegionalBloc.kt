package io.tusharnimbokar.countrylistapp.data.model

import java.io.Serializable

data class RegionalBloc(
    val acronym: String,
    val name: String,
    val otherAcronyms: List<Any>,
    val otherNames: List<String>
): Serializable