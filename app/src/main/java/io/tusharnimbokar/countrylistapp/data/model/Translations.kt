package io.tusharnimbokar.countrylistapp.data.model

import java.io.Serializable

data class Translations(
    val br: String,
    val de: String,
    val es: String,
    val fa: String,
    val fr: String,
    val hr: String,
    val `it`: String,
    val ja: String,
    val nl: String,
    val pt: String
): Serializable