package io.tusharnimbokar.countrylistapp.data.model.user

data class NameValuePairsX(
    val text: String,
    val title: String
)