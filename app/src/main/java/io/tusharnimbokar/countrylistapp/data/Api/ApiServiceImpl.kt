package io.tusharnimbokar.countrylistapp.data.Api

import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Single
import io.tusharnimbokar.countrylistapp.data.model.CountryListItem

class ApiServiceImpl : ApiService {

    override fun getCountry(): Single<List<CountryListItem>> {
        return Rx2AndroidNetworking.get("https://restcountries.eu/rest/v2/all")
            .build()
            .getObjectListSingle(CountryListItem::class.java)
    }

}