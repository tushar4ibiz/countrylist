package io.tusharnimbokar.countrylistapp.data.model.user

data class Data(
    val nameValuePairs: NameValuePairsX
)