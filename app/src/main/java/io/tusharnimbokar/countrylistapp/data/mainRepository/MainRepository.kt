package io.tusharnimbokar.countrylistapp.data.mainRepository

import io.reactivex.Single
import io.tusharnimbokar.countrylistapp.data.Api.ApiHelper
import io.tusharnimbokar.countrylistapp.data.model.CountryListItem

class MainRepository(private val apiHelper: ApiHelper) {

    fun getCountry(): Single<List<CountryListItem>> {
        return apiHelper.getCountry()
    }

}