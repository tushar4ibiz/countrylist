package io.tusharnimbokar.countrylistapp.data.model.user

data class NameValuePairsXX(
    val body: String,
    val icon: String,
    val priority: String,
    val sound: String,
    val tag: String,
    val title: String
)