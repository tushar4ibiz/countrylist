package io.tusharnimbokar.countrylistapp.data.Api

class ApiHelper(private val apiService: ApiService) {
    fun getCountry() = apiService.getCountry()

}