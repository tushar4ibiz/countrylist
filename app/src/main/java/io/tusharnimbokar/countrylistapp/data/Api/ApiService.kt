package io.tusharnimbokar.countrylistapp.data.Api

import io.reactivex.Single
import io.tusharnimbokar.countrylistapp.data.model.CountryListItem


interface ApiService {

    fun getCountry(): Single<List<CountryListItem>>

}