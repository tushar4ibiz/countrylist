package io.tusharnimbokar.countrylistapp.data.model.user

data class NameValuePairs(
    val `data`: Data,
    val notification: Notification,
    val to: String
)